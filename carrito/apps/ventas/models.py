from django.db import models
from carrito.apps.cliente.models import cliente

class categoriaProducto(models.Model):
	nombre = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nombre

class producto(models.Model):

	def url(self,filename):
		ruta = "files/images/producto/%s/%s"%(self.nombre,filename)
		return ruta

	categorias	= models.ForeignKey(categoriaProducto)
	nombre		= models.CharField(max_length=200)
	precio		= models.DecimalField(max_digits=6,decimal_places=2)
	stock		= models.IntegerField()
	status		= models.BooleanField(default=True)
	imagen		= models.ImageField(upload_to=url,null=True,blank=True)

	def __unicode__(self):
		return self.nombre

class compra(models.Model):
	idcliente	= models.ForeignKey(cliente)
	fecha 		= models.DateTimeField(auto_now_add=True, blank=True)

	def __unicode__(self):
		return "%s - %s"%(unicode(self.idcliente),self.fecha.replace(microsecond=0))

class detalleCompra(models.Model):
	idcompra		= models.ForeignKey(compra)
	idproducto	= models.ForeignKey(producto)
	cantidad	= models.IntegerField()
	precio		= models.DecimalField(max_digits=6,decimal_places=2)

	def __unicode__(self):
		return "%s - %s"%(self.idcompra,self.idproducto)