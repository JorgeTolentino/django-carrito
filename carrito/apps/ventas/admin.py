from django.contrib import admin
from carrito.apps.ventas.models import producto,categoriaProducto,detalleCompra,compra

admin.site.register(categoriaProducto)
admin.site.register(producto)
admin.site.register(compra)
admin.site.register(detalleCompra)