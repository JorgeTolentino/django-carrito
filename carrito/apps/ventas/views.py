from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from carrito.apps.ventas.models import producto, compra, detalleCompra
from carrito.apps.cliente.models import cliente
from carrito.apps.ventas.forms import addCompra
from django.core.urlresolvers import reverse

def index(request):
	return HttpResponseRedirect(reverse("lista_productos"))

def lista_productos_view(request):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	_usuario = request.session['cliente']
	results = producto.objects.filter(status=True,stock__gt=0)
	mi_carrito = {}
	temp_mi_carrito = {}
	suma = 0
	mensaje = ""
	if 'carrito' in request.session:
		mi_carrito = request.session['carrito']
		for k,v in mi_carrito.items():
			lc = producto.objects.filter(id=k)
			if lc:
				lc = lc[0]
				color = ("red","green")[lc.stock>=v["cantidad"]]
				temp_mi_carrito[k] = {'id':k,'nombre':lc.nombre,'stock':lc.stock,'precio':lc.precio,'cantidad':v["cantidad"],'total':(lc.precio*v["cantidad"]), 'color':color}
				suma += (lc.precio*v["cantidad"])

	if 'mensaje' in request.session:
		mensaje = request.session['mensaje']
		del request.session['mensaje']

	ctx = {'results':results,'carrito':temp_mi_carrito,'suma':suma, 'mensaje':mensaje}
	return render_to_response('ventas/productos.lista.html',ctx, context_instance=RequestContext(request))

def agregar_al_carrito_view(request, id):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	existe = producto.objects.filter(status=True,pk=id)
	if existe:
		existe = existe[0]
		cantidad = 1

		msg_except = ""

		if request.method == "POST":
			post = request.POST
			try:
			    cantidad = int(post["cantidad"])
			except ValueError:
				msg_except = '<p class="error">La cantidad debe ser un numero entero. (Se ingreso 1 como cantidad)</p>'

		if msg_except != "":
			cantidad = 1	

		if cantidad < 1:
			request.session['mensaje'] = '<p class="error">La cantidad no puede ser menor a 1.</p>'
			return HttpResponseRedirect(reverse("lista_productos"))			    

		if cantidad > existe.stock:
			request.session['mensaje'] = '<p class="error">La cantidad que desea comprar supera el stock del producto.</p>'
			return HttpResponseRedirect(reverse("lista_productos"))

		mi_carrito = {}
		if 'carrito' not in request.session:
			mi_carrito[id] = {"cantidad":cantidad}
		else:
			mi_carrito = request.session['carrito']
			mi_carrito[id] = {"cantidad":cantidad}

		request.session['mensaje'] = '<p class="success">Se registro el producto en su carrito.</p>' + msg_except
		request.session['carrito'] = mi_carrito

	return HttpResponseRedirect(reverse("lista_productos"))

def borrar_uno_carrito_view(request, id):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	mi_carrito = {}
	if 'carrito' in request.session:
		mi_carrito = request.session['carrito']
		if id in mi_carrito:
			del mi_carrito[id]
			request.session['mensaje'] = '<p class="success">El producto se ha eliminado.</p>'
	request.session['carrito'] = mi_carrito
	return HttpResponseRedirect(reverse("lista_productos"))

def guardar_compra_view(request):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	_usuario = request.session['cliente']

	if 'carrito' in request.session:
		mi_carrito = request.session['carrito']

	mCompra = compra()
	mCompra.idcliente = cliente.objects.get(id=_usuario["idcliente"])
	mCompra.save()
	mensaje_no_registro = ""
	mensaje_guardo = ""

	for k,v in mi_carrito.items():
		mProducto = producto.objects.get(id=k)
		if mProducto.status == True:
			if mProducto.stock>=v["cantidad"]:

				mdetalleCompra = detalleCompra()
				mdetalleCompra.idcompra = mCompra 
				mdetalleCompra.idproducto = mProducto
				mdetalleCompra.cantidad = v["cantidad"]
				mdetalleCompra.precio = mProducto.precio
				mdetalleCompra.save()

				mProducto.stock = (mProducto.stock - v["cantidad"])
				mProducto.save()
				mensaje_guardo = '<p class="success">Se guardo su compra.</p>'
			else:
				mensaje_no_registro += '<p class="error">la cantidad(%s) que desea comprar para el producto: %s supera a la cantidad(%s) actual en almacen.</p>'%(v["cantidad"],mProducto.nombre,mProducto.stock)

	del request.session['carrito']
	request.session['mensaje'] = '%s %s'%(mensaje_guardo,mensaje_no_registro)
	return HttpResponseRedirect(reverse("lista_productos"))

def mis_compras_view(request):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))

	_usuario = request.session['cliente']
	com = compra.objects.order_by('-fecha').filter(idcliente=_usuario["idcliente"])
	ctx = {'compra':com}
	return render_to_response('ventas/miscompras.html',ctx, context_instance=RequestContext(request))