from django.conf.urls.defaults import patterns,url

urlpatterns = patterns('carrito.apps.ventas.views',
		url(r'^$', 'index',name='index'),
		url(r'^productos/$', 'lista_productos_view',name='lista_productos'),
		url(r'^agregar_al_carrito/(?P<id>.*)$', 'agregar_al_carrito_view',name='agregar_al_carrito'),
		url(r'^borrar_uno_carrito/(?P<id>.*)$', 'borrar_uno_carrito_view',name='borrar_uno_carrito'),
		url(r'^guardar_compra/$', 'guardar_compra_view',name='guardar_compra'),
		url(r'^miscompras/$', 'mis_compras_view',name='mis_compras'),
)