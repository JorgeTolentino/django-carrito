from django.conf.urls.defaults import patterns,url

urlpatterns = patterns('carrito.apps.cliente.views',
		url(r'^logincliente/$', 'loginCliente_view',name='login_cliente'),
		url(r'^logoutcliente/$', 'logoutCliente_view',name='logout_cliente'),
		url(r'^registrocliente/$', 'registroCliente_view',name='registro_Cliente'),
		url(r'^editarcliente/$', 'editarCliente_view',name='editar_Cliente'),
		url(r'^cambiarclave/$', 'cambiarClave_view',name='cambiar_clave'),
)