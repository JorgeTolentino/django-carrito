# coding: utf-8
from django.db import models
from django.core.exceptions import ValidationError

def validate_length_6(value,length=6):
    if length>len(str(value)):
        raise ValidationError('Asegúrese de que este valor tiene al menos 6 caracteres (actualmente tiene %s)' %  len(str(value)))
       
class cliente(models.Model):

	def url(self,filename):
		ruta = "files/images/cliente/%s/%s"%(self.cuenta,filename)
		return ruta
	
	nombre		= models.CharField(verbose_name="Nombre",max_length=25)
	apellidoPaterno = models.CharField(verbose_name="Apellido paterno",max_length=45)
	apellidoMaterno = models.CharField(verbose_name="Apellido materno",max_length=45)
	cuenta		= models.CharField(verbose_name="Cuenta",max_length=20,validators=[validate_length_6])
	clave		= models.CharField(verbose_name="Clave",max_length=20)
	status		= models.BooleanField(verbose_name="Estado",default=True)
	imagen		= models.ImageField(verbose_name="Avatar",upload_to=url,null=True,blank=True)

	def __unicode__(self):
		return "Cuenta: %s - Nombre: %s"%(self.cuenta, (self.apellidoPaterno + " " + self.apellidoMaterno + ", " + self.nombre))