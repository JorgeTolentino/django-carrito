from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from carrito.apps.cliente.models import cliente
from carrito.apps.cliente.forms import LoginForm,RegistroForm,EditarForm,CambiarClaveForm
from django.core.urlresolvers import reverse

def registroCliente_view(request):
	if 'cliente' in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	form = RegistroForm()
	mensaje = ""
	if request.method == "POST":
		form = RegistroForm(request.POST,request.FILES)
		if form.is_valid():
			add = form.save(commit=False)
			add.status= True
			add.save()
			mensaje = '<p class="success">Se rergistro cliente.</p>'
			form = RegistroForm()
	ctx = {'form':form,'mensaje':mensaje}
	return render_to_response('cliente/registro.html',ctx, context_instance=RequestContext(request))

def editarCliente_view(request):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	_usuario = request.session['cliente']

	mensaje = ""
	cli = cliente.objects.get(pk=_usuario["idcliente"])
	form = EditarForm(instance=cli)
	if request.method == "POST":
		form = EditarForm(request.POST,request.FILES,instance=cli)
		if form.is_valid():
			form.save()
			mensaje = '<p class="success">Se edito su informacion.</p>'
	
	ctx = {'form':form,'mensaje':mensaje}
	return render_to_response('cliente/editarcliente.html',ctx, context_instance=RequestContext(request))

def cambiarClave_view(request):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	_usuario = request.session['cliente']

	mensaje = ""
	cli = cliente.objects.get(pk=_usuario["idcliente"])
	form = CambiarClaveForm(instance=cli)
	if request.method == "POST":
		form = CambiarClaveForm(request.POST,request.FILES,instance=cli)
		if form.is_valid():
			form.save()
			mensaje = '<p class="success">Se cambio su clave.</p>'

	ctx = {'form':form,'mensaje':mensaje}
	return render_to_response('cliente/cambiarclave.html',ctx, context_instance=RequestContext(request))

def loginCliente_view(request):
	mensaje = ""
	if 'cliente' in request.session:
		return HttpResponseRedirect(reverse("lista_productos"))
	else:
		form = LoginForm()
		if request.method == "POST":
			form = LoginForm(request.POST)
			if form.is_valid():
				usuario = form.cleaned_data['usuario']
				clave	= form.cleaned_data['clave']
				existe = cliente.objects.filter(cuenta__iexact=usuario,clave=clave)
				if existe:
					existe = existe[0]
					request.session['cliente'] = {'idcliente':existe.id,'cuenta':existe.cuenta,'nombre':(existe.apellidoPaterno + ' ' + existe.apellidoMaterno + ', ' + existe.nombre),'imagen':existe.imagen} 
					return HttpResponseRedirect(reverse("lista_productos"))
				else:
					mensaje = '<p class="error">usuario y/o password incorrecto.</p>'
		ctx = {'form':form,'mensaje':mensaje}
		return render_to_response('cliente/login.html',ctx,context_instance=RequestContext(request))	

def logoutCliente_view(request):
	request.session.clear()
	return HttpResponseRedirect(reverse("login_cliente"))
