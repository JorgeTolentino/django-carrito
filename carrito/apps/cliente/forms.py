# coding: utf-8
from django import forms
from django.contrib.auth.models import User
from carrito.apps.cliente.models import cliente
from django import forms

class LoginForm(forms.Form):
	usuario = forms.CharField(widget=forms.TextInput(attrs={'autocomplete':'off'}))
	clave = forms.CharField(widget=forms.PasswordInput(render_value=False))

class RegistroForm(forms.ModelForm):
	class Meta:
		model = cliente
		fields = ['nombre', 'apellidoPaterno', 'apellidoMaterno', 'cuenta', 'imagen','clave']
		widgets = {
            'clave': forms.PasswordInput(),
        }
	clave_dos = forms.CharField(label="Confirmar clave",widget=forms.PasswordInput(render_value=False))

	def clean_cuenta(self):
		cuenta = self.cleaned_data['cuenta']
		if ' ' in cuenta:
			raise forms.ValidationError("Espacios no permitidos en el campo cuenta.")
		try:
			c = cliente.objects.get(cuenta=cuenta)
		except cliente.DoesNotExist:
			return cuenta
		raise forms.ValidationError('Cuenta ya registrada')

	def clean_clave(self):
		clave_uno = self.cleaned_data['clave']
		if clave_uno and 6 > len(clave_uno):
			raise forms.ValidationError('Asegúrese de que este valor tiene al menos 6 caracteres (actualmente tiene %s)' %  len(str(clave_uno)))
		return clave_uno
	

	def clean_clave_dos(self):
		clave_uno = self.cleaned_data.get('clave')
		clave_dos = self.cleaned_data.get('clave_dos')
		if clave_uno and clave_uno != clave_dos:
			raise forms.ValidationError('Claves no coinciden')
		return clave_dos

class EditarForm(forms.ModelForm):
	class Meta:
		model = cliente
		fields = ['nombre', 'apellidoPaterno', 'apellidoMaterno', 'imagen']

class CambiarClaveForm(forms.ModelForm):
	class Meta:
		model = cliente
		fields = ['clave']
		widgets = {
            'clave': forms.PasswordInput(),
        }
	clave_dos = forms.CharField(label="Confirmar clave",widget=forms.PasswordInput(render_value=False))

	def clean_clave(self):
		clave_uno = self.cleaned_data['clave']
		if clave_uno and 6 > len(clave_uno):
			raise forms.ValidationError('Asegúrese de que este valor tiene al menos 6 caracteres (actualmente tiene %s)' %  len(str(clave_uno)))
		return clave_uno
	
	def clean_clave_dos(self):
		clave_uno = self.cleaned_data.get('clave')
		clave_dos = self.cleaned_data.get('clave_dos')
		if clave_uno and clave_uno != clave_dos:
			raise forms.ValidationError('Claves no coinciden')
		return clave_dos