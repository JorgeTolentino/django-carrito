from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from carrito.apps.cliente.models import cliente

def my_processor(request):
	if 'cliente' not in request.session:
		return HttpResponseRedirect(reverse("login_cliente"))
	_usuario = request.session['cliente']

	cli = cliente.objects.get(id=_usuario["idcliente"])

	context = {
		"cliente_cuenta": cli.cuenta,
		"cliente_nombre": (cli.apellidoPaterno + ' ' + cli.apellidoMaterno + ', ' + cli.nombre),
		"cliente_imagen": cli.imagen
	}
	return context